#set_property PACKAGE_PIN C8   [get_ports TDM_A_0_P] #E2
#set_property PACKAGE_PIN C7   [get_ports TDM_A_0_N] #E3
#set_property PACKAGE_PIN J11  [get_ports TDM_A_1_P] #E6
#set_property PACKAGE_PIN H11  [get_ports TDM_A_1_N] #E7
#set_property PACKAGE_PIN B10  [get_ports TDM_A_2_P] #E9
#set_property PACKAGE_PIN A10  [get_ports TDM_A_2_N] #E10
#set_property PACKAGE_PIN J10  [get_ports TDM_A_3_P] #E12
#set_property PACKAGE_PIN J9   [get_ports TDM_A_3_N] #E13

#set_property PACKAGE_PIN K8   [get_ports TDM_O_EN_0] #E15
#set_property PACKAGE_PIN K7   [get_ports TDM_O_EN_1] #E16
#set_property PACKAGE_PIN B2   [get_ports TDM_O_EN_2] #E18
#set_property PACKAGE_PIN A2   [get_ports TDM_O_EN_3] #E19


set_property IOSTANDARD LVDS [get_ports TDM_O_P[*]]
set_property IOSTANDARD LVDS [get_ports TDM_O_N[*]]

set_property IOSTANDARD LVCMOS18 [get_ports TDM_O_EN[0]]
set_property IOSTANDARD LVCMOS18 [get_ports TDM_O_EN[1]]
set_property IOSTANDARD LVCMOS18 [get_ports TDM_O_EN[2]]
set_property IOSTANDARD LVCMOS18 [get_ports TDM_O_EN[3]]

set_property PACKAGE_PIN C8 [get_ports  TDM_O_P[0]]
set_property PACKAGE_PIN J11 [get_ports TDM_O_P[1]]
set_property PACKAGE_PIN B10 [get_ports TDM_O_P[2]]
set_property PACKAGE_PIN J10 [get_ports TDM_O_P[3]]

set_property PACKAGE_PIN K8 [get_ports TDM_O_EN[0]]
set_property PACKAGE_PIN K7 [get_ports TDM_O_EN[1]]
set_property PACKAGE_PIN B2 [get_ports TDM_O_EN[2]]
set_property PACKAGE_PIN A2 [get_ports TDM_O_EN[3]]

set_property PACKAGE_PIN J15 [get_ports {VADJ[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {VADJ[0]}]

##HA17_N
set_property PACKAGE_PIN L4 [get_ports pps]
set_property IOSTANDARD LVCMOS18 [get_ports pps] 

##Copied from Interposer_SW.xdc
set_property PACKAGE_PIN N3 [get_ports {PAMP_ID[0]}]
set_property PACKAGE_PIN N2 [get_ports {PAMP_ID[1]}]
set_property PACKAGE_PIN M6 [get_ports {PAMP_ID[2]}]
set_property PACKAGE_PIN M5 [get_ports {PAMP_ID[3]}]
set_property PACKAGE_PIN M2 [get_ports {PAMP_ID[4]}]
set_property PACKAGE_PIN L2 [get_ports {PAMP_ID[5]}]
set_property PACKAGE_PIN N4 [get_ports {PAMP_ID[6]}]
set_property PACKAGE_PIN M4 [get_ports {PAMP_ID[7]}]

set_property IOSTANDARD LVCMOS18 [get_ports {PAMP_ID[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PAMP_ID[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PAMP_ID[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PAMP_ID[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PAMP_ID[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PAMP_ID[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PAMP_ID[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PAMP_ID[7]}]
