library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

entity SIMPLE_PTP is
  generic(
    ADDRRES  : integer :=8;
    TYPERES  : integer :=8;
    TSRES    : integer :=32;
    
    cpb     : integer :=5; --Clock per bit for UART Communication
    
    HVIBNISWT : integer :=1000; --HVIBNI Switch time in clock cycles
    MAXTIMEOUT  : integer :=100000);
  port (
    CLK : IN STD_LOGIC;
    RSTN : IN STD_LOGIC;
    ENABLE : IN STD_LOGIC; --Enable PTP block, also load initial configuration.

    MSFLAG : IN STD_LOGIC; --Master or Slave Flag
    MSOUTFLAG : OUT STD_LOGIC; --Master or Slave Current Configuration
    
    LOCAL_TIME : IN STD_LOGIC_VECTOR(TSRES -1 DOWNTO 0); --LOCAL_TIME
    CTIME : OUT STD_LOGIC_VECTOR(TSRES -1 DOWNTO 0); --COMPENSATED_TIME
    CTRDY : OUT STD_LOGIC; --Compensated Time Ready
    
    ADDR : IN STD_LOGIC_VECTOR(ADDRRES-1 downto 0); --ADDR    

    --HVIBNIxN Driver Signals
    D_RX  : IN STD_LOGIC_VECTOR(4-1 DOWNTO 0); --Data buffer Rx
    D_TX  : OUT STD_LOGIC_VECTOR(4-1 DOWNTO 0); --Data buffer Tx
    D_DIR  : OUT STD_LOGIC_VECTOR(4-1 DOWNTO 0); --Data Buffer Direction 1 Input, 0 Output

    --MASTER ONLY PORTS
    Start_SYNC : IN STD_LOGIC;

    DBG_STATE : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    DBG_hvibni_to_done : OUT STD_LOGIC;
    DBG_utx_o_TX_Done : OUT STD_LOGIC;
    DBG_urx_o_RX_DV : OUT STD_LOGIC;
    DBG_tmout_done : OUT STD_LOGIC;
    DBG_t1 : OUT STD_LOGIC_VECTOR(TSRES -1 DOWNTO 0);
    DBG_t2 : OUT STD_LOGIC_VECTOR(TSRES -1 DOWNTO 0);
    DBG_t3 : OUT STD_LOGIC_VECTOR(TSRES -1 DOWNTO 0);
    DBG_t4 : OUT STD_LOGIC_VECTOR(TSRES -1 DOWNTO 0);
    DBG_delta : OUT STD_LOGIC_VECTOR(TSRES -1 DOWNTO 0);
    DBG_ptprx_ptype : OUT STD_LOGIC_VECTOR(TYPERES -1  DOWNTO 0);
    DBG_ptprx_addr : OUT STD_LOGIC_VECTOR(ADDRRES -1 DOWNTO 0);
    DBG_ptprx_ts : OUT STD_LOGIC_VECTOR(TSRES -1 DOWNTO 0)
    
  ) ;
end SIMPLE_PTP ; 

architecture arch of SIMPLE_PTP is

--!Components Definitions

component fall_edge_detector is
  port (
      clk                       : in  std_logic;
      rstn                      : in  std_logic;
      s                         : in  std_logic;
      pulse                     : out std_logic);
end component fall_edge_detector;

component rising_edge_detector is
  port (
    clk                       : in  std_logic;
    rstn                      : in  std_logic;
    s                         : in  std_logic;
    pulse                     : out std_logic);
end component rising_edge_detector;

component timestamp is
  generic(CLK_RES : INTEGER :=TSRES);
  port (
    CLK  : IN STD_LOGIC;
    RSTN : IN STD_LOGIC;

    C_COUNT : IN STD_LOGIC_VECTOR(CLK_RES-1 DOWNTO 0);
    TS : OUT STD_LOGIC_VECTOR(CLK_RES - 1 DOWNTO 0);
    
    
    STAMPED : OUT STD_LOGIC; --STAMPED flag high until CLEAR is received. 
    CLEAR : IN STD_LOGIC; --CLEAR timestamp 
    STAMP : IN STD_LOGIC --STAMP signal
  ) ;
end component timestamp ; 

component timeout is
  port (
    CLK : IN STD_LOGIC;
    RSTN : IN STD_LOGIC;
    START : IN STD_LOGIC;
    CLEAR : IN STD_LOGIC;

    COUNT : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    DONE  : OUT STD_LOGIC
  ) ;
end component timeout ; 

component UART_RX is
  generic (
    g_CLKS_PER_BIT : integer := cpb;     -- Needs to be set correctly
    NBITS : integer :=ADDRRES + TYPERES + TSRES
    );
  port (
    i_Clk       : in  std_logic;
    i_RX_EN     : in  std_logic;
    i_RX_Serial : in  std_logic;
    o_RX_DV     : out std_logic;
    o_RX_Byte   : out std_logic_vector((NBITS -1) downto 0)
    );
end component UART_RX;

component UART_TX is
  generic (
    g_CLKS_PER_BIT : integer := cpb;     -- Needs to be set correctly clk_freq/
    NBITS : integer :=ADDRRES + TYPERES + TSRES 
    );
  port (
    i_Clk       : in  std_logic;
    i_TX_DV     : in  std_logic;
    i_TX_Byte   : in  std_logic_vector(NBITS -1 downto 0);
    o_TX_Active : out std_logic;
    o_TX_Serial : out std_logic;
    o_TX_Done   : out std_logic
    );
end component UART_TX;

constant PTP_PKT_SIZE : integer := ADDRRES + TYPERES + TSRES;
constant MASTER_HVIBNI_CFG : STD_LOGIC_VECTOR(4 -1 DOWNTO 0) := "0011";
constant SLAVETX_HVIBNI_CFG : STD_LOGIC_VECTOR(4 -1 DOWNTO 0) := "1100";
constant DEFAULT_HVIBNI_CFG : STD_LOGIC_VECTOR(4 -1 DOWNTO 0) := "1111"; --Default Configuration All Channels as INPUT

--type of messages for header
constant ptp_sync     : STD_LOGIC_VECTOR := "00010001";
constant ptp_dly_req  : STD_LOGIC_VECTOR := "00100010";
constant ptp_dly_resp : STD_LOGIC_VECTOR := "00110011";

--States definitions and signals
type state is (
  --Comon States
  Init,
  --Master States
  Master,
  Tx_Sync,
  Rx_Delay_Req,
  Tx_Delay_Resp,
  --Slave States
  Slave,
  Rx_Sync,
  Tx_Delay_Req,
  Rx_Delay_Resp
);

signal cstate, nstate : state;

--Tx Rx signals
type UTX_RECORD is record
  i_TX_DV     : std_logic;
  i_TX_Byte   : std_logic_vector(PTP_PKT_SIZE -1 downto 0);
  o_TX_Active : std_logic;
  o_TX_Serial : std_logic;
  o_TX_Done   : std_logic;
  i_TX_DV_Pulse : std_logic; --i_TX_DV for one clock cycle only
end record UTX_RECORD;

type URX_RECORD is record
  i_RX_EN     : std_logic;
  i_RX_Serial : std_logic;
  o_RX_DV     : std_logic; --Ready signal for one clock cycle
  o_RX_Byte   : std_logic_vector((PTP_PKT_SIZE -1) downto 0);
end record URX_RECORD;

type TS_RECORD is record
    ts : std_logic_vector(TSRES -1 DOWNTO 0);
    trig : std_logic;
    clear : std_logic;
    isUp : std_logic;
end record TS_RECORD;

type PTP_PACKAGE_RECORD is record
  ptype : std_logic_vector(TYPERES-1 downto 0);
  addr  : std_logic_vector(ADDRRES-1 downto 0);
  ts    : std_logic_vector(TSRES-1 downto 0);
end record PTP_PACKAGE_RECORD;

type TIMEOUT_RECORD is record
  start : std_logic;
  clear : std_logic;
  done  : std_logic;
end record TIMEOUT_RECORD;

signal utx : UTX_RECORD;
signal urx : URX_RECORD;
signal iTimeStamp, oTimestamp : TS_RECORD;
signal ptprx : PTP_PACKAGE_RECORD;
signal tmout, hvibni_to : TIMEOUT_RECORD;

signal msconfig : std_logic; --master slave configuration
signal ctime_flag :std_logic; --Compensated time flag

signal tm, t1, t2, t3, t4, delta : unsigned(TSRES -1 downto 0);
signal t1s, t2s, t3s, t4s : unsigned(TSRES -1 downto 0); --tx signals

signal tsum : unsigned(TSRES downto 0);

signal nSync : unsigned(31 downto 0); --Number of synchronizations for statistics

begin

  --Components Instantiation
  UTX_COMP: UART_TX
    generic map (
      g_CLKS_PER_BIT => cpb,     -- Needs to be set correctly clk_freq/
      NBITS => PTP_PKT_SIZE 
      )
    port map(
      i_Clk       => CLK,
      i_TX_DV     => utx.i_TX_DV_Pulse,
      i_TX_Byte   => utx.i_TX_Byte,
      o_TX_Active => utx.o_TX_Active,
      o_TX_Serial => utx.o_TX_Serial,
      o_TX_Done   => utx.o_TX_Done
      );

  URX_COMP: UART_RX
    generic map (
      g_CLKS_PER_BIT => cpb,     -- Needs to be set correctly
      NBITS => PTP_PKT_SIZE 
      )
    port map(
      i_Clk       => CLK,
      i_RX_EN     => urx.i_RX_EN,
      i_RX_Serial => urx.i_RX_Serial,
      o_RX_DV     => urx.o_RX_DV,
      o_RX_Byte   => urx.o_RX_Byte
      );

  FEDGE_COMP : fall_edge_detector
    port map (
      clk   => CLK,
      rstn  => RSTN,
      s     => urx.i_RX_Serial,
      pulse => iTimeStamp.trig
      );

  REDGE_COMP : rising_edge_detector
    port map (
      clk   => CLK,
      rstn  => RSTN,
      s     => utx.i_TX_DV,
      pulse => utx.i_TX_DV_Pulse
      );

  iTS_GEN : timestamp
    generic map(CLK_RES => TSRES)
    port map(
      CLK     => CLK, 
      RSTN    => RSTN, 
      C_COUNT => LOCAL_TIME, 
      TS      => iTimeStamp.ts, 
      STAMPED => iTimeStamp.isUp, 
      CLEAR   => iTimeStamp.clear, 
      STAMP   => iTimeStamp.trig
    ) ;

  oTS_GEN : timestamp
    generic map(CLK_RES => TSRES)
    port map(
      CLK     => CLK, 
      RSTN    => RSTN, 
      C_COUNT => LOCAL_TIME, 
      TS      => oTimeStamp.ts, 
      STAMPED => oTimeStamp.isUp, 
      CLEAR   => oTimeStamp.clear, 
      STAMP   => utx.i_TX_DV
    ) ;

    
  TIMEOUT_COMP: timeout
      port map (
      CLK   => CLK,
      RSTN  => RSTN,
      START => tmout.start,
      CLEAR => tmout.clear,
      COUNT => STD_LOGIC_VECTOR(TO_UNSIGNED(MAXTIMEOUT,32)),
      DONE  => tmout.done
    ) ;

  HVIBNI_TIMEOUT: timeout
      port map (
      CLK   => CLK,
      RSTN  => RSTN,
      START => hvibni_to.start,
      CLEAR => hvibni_to.clear,
      COUNT => STD_LOGIC_VECTOR(TO_UNSIGNED(HVIBNISWT,32)),
      DONE  => hvibni_to.done
    ) ;

--FSM processes 
  --State change process
  process(CLK, RSTN)
  begin
    if RSTN = '0' then
      cstate<=Init;
    elsif rising_edge(CLK) then
      if ENABLE ='0' THEN
        cstate<=Init;
      else 
        cstate<=nstate;
      end if;
    end if;
  end process;

  process(CLK,RSTN)
  begin
    if RSTN='0' then
      t1s<=(others=>'0');
      t2s<=(others=>'0');
      t3s<=(others=>'0');
      t4s<=(others=>'0'); 
    elsif rising_edge(CLK) then
      t1s<=t1;
      t2s<=t2;
      t3s<=t3;
      t4s<=t4;
    end if;
  end process;

  --Current state process
  process(cstate, MSFLAG)
  begin
    case cstate is
  --Comon States
      when Init =>
        CTRDY<='0'; --Compensated Time not ready
        msconfig<='0';
        D_DIR<=DEFAULT_HVIBNI_CFG; --All inputs configuration
        --TX          
        utx.i_TX_DV <= '0'; --No TX enable
        utx.i_TX_Byte <= (others=>'0'); --Empty TX Bytes
        --RX 
        urx.i_RX_EN<='0'; --Disable RX
        --Timestamp Control
        iTimestamp.clear<='1';
        oTimeStamp.clear<='1';
        --Timeout Control
        tmout.start<='0';
        tmout.clear<='1';
        hvibni_to.start<='0';
        hvibni_to.clear<='1';
        --TIME FLAGS
        t1<=(others=>'0');
        t2<=(others=>'0');
        t3<=(others=>'0');
        t4<=(others=>'0');        
  --Master States 
      when Master =>
        CTRDY<='1'; --Ready to start sync
        msconfig<='1';
        D_DIR<=MASTER_HVIBNI_CFG; --Direction Config
        --TX          
        utx.i_TX_DV <= '0'; --No TX enable
        utx.i_TX_Byte <= (others=>'0'); --Empty TX Bytes
        --RX 
        urx.i_RX_EN<='0'; --Disable RX  
        --Timestamp Control
        iTimestamp.clear<='1';
        oTimeStamp.clear<='1';
        --Timeout Control
        tmout.start<='0';
        tmout.clear<='1';
        hvibni_to.start<='0';
        hvibni_to.clear<='1';
        --TIME FLAGS
        t1<=(others=>'0');
        t2<=(others=>'0');
        t3<=(others=>'0');
        t4<=(others=>'0');          
      when Tx_Sync =>
        CTRDY<='0'; --Compensated Time not ready
        msconfig<='1';
        D_DIR<=MASTER_HVIBNI_CFG; --Direction Config
        --TX          
        utx.i_TX_DV <= '1'; --TX enable for one clock cycle
        utx.i_TX_Byte <= ptp_sync & ADDR & LOCAL_TIME; --Sync signal, to address and local time
        --RX 
        urx.i_RX_EN<='0'; 
        --Timestamp Control
        iTimestamp.clear<='0';
        oTimeStamp.clear<='1';
        --Timeout Control
        tmout.start<='0';
        tmout.clear<='1';
        hvibni_to.start<='0';
        hvibni_to.clear<='1';
        --TIME FLAGS
        t1<=(others=>'0');
        t2<=(others=>'0');
        t3<=(others=>'0');
        t4<=(others=>'0');          
      when Rx_Delay_Req =>
        CTRDY<='0'; --Compensated Time not ready
        msconfig<='1';
        D_DIR<=MASTER_HVIBNI_CFG; --Direction Config
        --TX          
        utx.i_TX_DV <= '0'; --No TX enable
        utx.i_TX_Byte <= ptp_dly_resp & ADDR & iTimestamp.ts; --Sync signal, to address and local time
        --RX 
        urx.i_RX_EN<='1'; --RX enable
        --Timestamp Control
        iTimestamp.clear<='0';
        oTimeStamp.clear<='1';
        --Timeout Control
        tmout.start<='1';
        tmout.clear<='0';
        hvibni_to.start<='0';
        hvibni_to.clear<='1';
        --TIME FLAGS
        t1<=(others=>'0');
        t2<=(others=>'0');
        t3<=(others=>'0');
        t4<=(others=>'0');  
      when Tx_Delay_Resp =>
        CTRDY<='0'; --Compensated Time not ready
        msconfig<='1';
        D_DIR<=MASTER_HVIBNI_CFG; --Direction Config
        --TX          
        utx.i_TX_DV <= '1'; --No TX enable
        utx.i_TX_Byte <= ptp_dly_resp & ADDR & iTimestamp.ts; --Sync signal, to address and local time
        --RX 
        urx.i_RX_EN<='0'; --Disable RX
        --Timestamp Control
        iTimestamp.clear<='0';
        oTimeStamp.clear<='1';
        --Timeout Control
        tmout.start<='0';
        tmout.clear<='0';
        hvibni_to.start<='0';
        hvibni_to.clear<='1';
        --TIME FLAGS
        t1<=(others=>'0');
        t2<=(others=>'0');
        t3<=(others=>'0');
        t4<=(others=>'0');        
  --Slave State 
      when Slave =>
        CTRDY<=ctime_flag; --Compensated Time not ready
        msconfig<='0';
        D_DIR<=DEFAULT_HVIBNI_CFG; --All inputs
        --TX          
        utx.i_TX_DV <= '0'; --No TX enable
        utx.i_TX_Byte <= (others=>'0');
        --RX 
        urx.i_RX_EN<='1'; --Enable RX
        --Timestamp Control
        iTimestamp.clear<='0';
        oTimeStamp.clear<='1';
        --Timeout Control
        tmout.start<='0';
        tmout.clear<='0';
        hvibni_to.start<='0';
        hvibni_to.clear<='1';
        --TIME FLAGS
        t1<=t1s;
        t2<=t2s;
        t3<=t3s;
        t4<=t4s;      
      when Rx_Sync =>
        CTRDY<=ctime_flag; --Compensated Time not ready
        msconfig<='0';
        --TX          
        utx.i_TX_DV <= '0'; --No TX enable
        utx.i_TX_Byte <= (others=>'0');
        --RX 
        urx.i_RX_EN <= '1';
        --Timeout Control
        tmout.start<='0';
        tmout.clear<='1';
        hvibni_to.start<='1';
        hvibni_to.clear<='0';
        --TIME FLAGS
        oTimeStamp.clear<='1';
        if ptprx.addr = ADDR then --Addr match
          --Timestamp Control
          iTimestamp.clear<='0';
          t1<=unsigned(ptprx.ts)-2; --Compensating for 2 clock cycles of latency since start signal was sent
          t2<=unsigned(iTimestamp.ts);
          D_DIR<=SLAVETX_HVIBNI_CFG; --Slave TX Configuration   
        else
          --Timestamp Control
          iTimestamp.clear<='1';         
          t1<=t1s;
          t2<=t2s;
          D_DIR<=DEFAULT_HVIBNI_CFG; --All inputs
        end if; 
        t3<=t3s;
        t4<=t4s;
      when Tx_Delay_Req =>
        oTimeStamp.clear<='0';
        CTRDY<=ctime_flag; --Compensated Time not ready
        msconfig<='0';
        D_DIR<=SLAVETX_HVIBNI_CFG; --Slave TX Config
        --TX          
        utx.i_TX_DV <= '1'; --TX enable
        utx.i_TX_Byte <= ptp_dly_req & ADDR & oTimestamp.ts;
        --RX 
        urx.i_RX_EN<='1'; --Disable RX
        --Timestamp Control
        iTimestamp.clear<='1';
        --Timeout Control
        tmout.start<='0';
        tmout.clear<='0';
        hvibni_to.start<='0';
        hvibni_to.clear<='1';
        --TIME FLAGS
        t1<=t1s;
        t2<=t2s;
        t3<=unsigned(oTimestamp.ts)+3; --Store local time add 3 clock cycles of time correction
        t4<=t4s;  
      when Rx_Delay_Resp =>
        CTRDY<=ctime_flag; --Compensated Time not ready
        msconfig<='0';
        D_DIR<=SLAVETX_HVIBNI_CFG; --Slave TX Config
        --TX          
        utx.i_TX_DV <= '0'; --No TX enable
        utx.i_TX_Byte <= (others=>'0');
        --RX 
        urx.i_RX_EN<='1';
        --Timestamp Control
        iTimestamp.clear<='1';
        oTimeStamp.clear<='1';
        --Timeout Control
        tmout.start<='1';
        tmout.clear<='0';
        hvibni_to.start<='0';
        hvibni_to.clear<='1';
        --TIME FLAGS
        t1<=t1s;
        t2<=t2s;
        t3<=t3s;
        if ptprx.addr = ADDR and ptprx.ptype = ptp_dly_resp then 
          t4<=unsigned(ptprx.ts); --substracted 1 clock cycle of error
        else
          t4<=t4s;
        end if;
    end case;
  end process;

  --Next state processs
  process(cstate, MSFLAG, Start_SYNC, hvibni_to.done, utx.o_TX_Done, urx.o_RX_DV, tmout.done)
  begin
    case cstate is
    --Comon States
      when Init =>
        nSync<=(others=>'0');
        if MSFLAG = '1' then 
          nstate<=Master;
        else 
          nstate<=Slave;
        end if;
    --Master States
      when Master =>
        nSync<=nSync;
        if Start_SYNC = '1' then
          nstate<=Tx_Sync;
        else
          nstate<=Master;
        end if;
      when Tx_Sync =>
        nSync<=nSync;
        if MSFLAG = '0' then
          nstate<=Slave;
        elsif utx.o_TX_Done = '1' then
          nstate<=Rx_Delay_Req;
        else
          nstate<=Tx_Sync;
        end if;
      when Rx_Delay_Req =>
        nSync<=nSync;
        if MSFLAG = '0' then
          nstate<=Slave;
        elsif tmout.done = '1' then
          nstate<=Master;
        elsif urx.o_RX_DV = '1'and ptprx.ptype = ptp_dly_req then
          nstate<=Tx_Delay_Resp;
        else 
          nstate<=Rx_Delay_Req;
        end if;
      when Tx_Delay_Resp =>  
        if MSFLAG = '0' then
          nstate<=Slave;  
        elsif utx.o_TX_Done = '1' and Start_SYNC = '0' then 
          nSync<=nSync+1;
          nstate<=Master;
        else
          nstate<=Tx_Delay_Resp;
          nSync<=nSync;
        end if;
    --Slave State 
      when Slave =>
        nSync<=nSync;    
        if urx.o_RX_DV = '1' and ptprx.ptype = ptp_sync then
            nstate<=Rx_Sync;
        else 
          nstate<=Slave;
        end if;
      when Rx_Sync =>
        nSync<=nSync;
        if MSFLAG = '1' then
          nstate<=Master;
        elsif ptprx.addr /= ADDR then
          nstate<=Slave;
        elsif ptprx.addr = ADDR and hvibni_to.done = '1' then --Time delay to switch hvibni direction
          nstate<=Tx_Delay_Req;
        else
          nstate<=Rx_Sync;
        end if;          
      when Tx_Delay_Req =>
        nSync<=nSync;
        if MSFLAG = '1' then
          nstate<=Master;
        elsif utx.o_TX_Done = '1' then
          nstate<=Rx_Delay_Resp;
        else 
          nstate<=Tx_Delay_Req;
        end if;          
      when Rx_Delay_Resp =>
        if MSFLAG = '1' then
          nstate<=Master;
        elsif tmout.done = '1' then
          nstate<=slave;
          nSync<=nSync;
        elsif urx.o_RX_DV = '1' then
          nstate<=slave;
          nSync<=nSync+1;
        else 
          nstate<=Rx_Delay_Resp;
          nSync<=nSync;
        end if;
    end case;
  end process;
--End of FSM processes
  --Other processes 


  --Direct Definitions
  MSOUTFLAG<=msconfig;
  D_TX<= utx.o_TX_Serial & LOCAL_TIME(28) & "00" when msconfig = '1' else
         "000" & utx.o_TX_Serial;

  urx.i_RX_Serial<=D_RX(0) when msconfig = '1' else
                   D_RX(3);

  tm<=unsigned(oTimeStamp.ts)+2; --Local time plus 2 clock cycles to send the sync message

  --ptp unpackage
  ptprx.ptype<=urx.o_RX_Byte(PTP_PKT_SIZE-1 downto PTP_PKT_SIZE-TYPERES);
  ptprx.addr<=urx.o_RX_Byte(PTP_PKT_SIZE-TYPERES-1 downto PTP_PKT_SIZE-TYPERES-ADDRRES);
  ptprx.ts<=urx.o_RX_Byte(PTP_PKT_SIZE-TYPERES-ADDRRES-1 downto 0);
  
  ctime_flag<='1' when nSync>0 and cstate=Slave else '0';


  tsum<=(('0'&t2)+('0'&t3))-(('0'&t1)+('0'&t4));
  delta<=tsum(TSRES downto 1);

  CTIME<=STD_LOGIC_VECTOR(UNSIGNED(LOCAL_TIME)-delta);


  --Debug Signals REMOVE WHEN FINISH
  DBG_STATE<= "0000" when cstate=Init else
              "0001" when cstate=Master else
              "0010" when cstate=Tx_Sync  else
              "0011" when cstate=Rx_Delay_Req else
              "0100" when cstate=Tx_Delay_Resp  else
              "0101" when cstate=Slave  else
              "0110" when cstate=Rx_Sync  else
              "0111" when cstate=Tx_Delay_Req else
              "1000" when cstate=Rx_Delay_Resp;         

  DBG_hvibni_to_done<=hvibni_to.done;
  DBG_utx_o_TX_Done<=utx.o_TX_Done;
  DBG_urx_o_RX_DV<=urx.o_RX_DV;
  DBG_tmout_done<=tmout.done;
  DBG_t1<=STD_LOGIC_VECTOR(t1);
  DBG_t2<=STD_LOGIC_VECTOR(t2);
  DBG_t3<=STD_LOGIC_VECTOR(t3);
  DBG_t4<=STD_LOGIC_VECTOR(t4);
  DBG_delta<=STD_LOGIC_VECTOR(delta);
  DBG_ptprx_ptype<=ptprx.ptype;
  DBG_ptprx_addr<=ptprx.addr;
  DBG_ptprx_ts<=ptprx.ts;

end architecture ;