--! \file sync_mgr

--! Synchronization manager designed to set up the PTP synchronization among the slaves 

library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

entity sync_mgr is
    generic( REG_RES : integer :=32;
             ADDR_RES : integer :=8;
             NSLAVES : integer :=10);
  port (
    CLK  : IN std_logic;
    RSTN : IN  std_logic;
    --! \param CFG_REG  Configuration Register
    --! CFG_REG[0] OVERRIDE
    --! CFG_REG[1] ENABLE
    --! CFG_REG[2] MSFLAG MASTER=1 SLAVE=0

    CFG_REG : IN std_logic_vector(REG_RES -1 DOWNTO 0);

    --! PTP_ADDR APPLYING THE FIX OF THE SWITCH WORKAROUND
    ADDR : IN std_logic(ADDR_RES -1 DOWNTO 0);

    --! \param SYNC_FREQ Number of clock cycles before sending a SYNC signal.
    
    SYNC_FREQ : IN std_logic_vector(REG_RES -1 DOWNTO 0);

    --! \PTP configuration port
    
    SPTP_ENABLE : OUT std_logic;
    SPTP_MSFLAG : OUT std_logic;
    SPTP_START_SYNC : OUT std_logic;
    SPTP_ADDR : OUT std_logic_vector(ADDR_RES -1 DOWNTO 0);

    SPTP_CTRDY : IN std_logic
  ) ;
end sync_mgr ; 

architecture Behavioral of sync_mgr is

type states is (rst_state, idle, send_sync, next_sync); 
signal cstate, nstate : states;

signal enable : std_logic;
signal msflag, ismaster  : std_logic;
signal caddr : std_logic_vector(ADDR_RES -1 DOWNTO 0);

begin

    msflag<='1' when ADDR = (others=>'0') else '0';
    caddr<="0000" & ADDR(6) & ADDR(4) & ADDR(2)& ADDR(0);

    --Override process
    process(RSTN, CFG_REG(0))
    begin
        if RSTN = '0' then
            enable<='0';
            ismaster<=msflag;
        elsif CFG_REG(0) = '1' then
            enable<=CFG_REG(1);
            ismaster<=CFG_REG(2);
        else
            enable<='0';
            ismaster<=msflag;
        end if;
    end process;        

    
    -- State change process
    process(CLK, RSTN)
    begin
        if RSTN ='0' then
            cstate<=rst_state;
        elsif rising_edge(CLK) then
            if enable = '1' then
                cstate<=rst_state;            
            else
                cstate<=nstate;
            end if;
        end if;
    end process;

    --Current state process
    process(cstate)
    begin
        case cstate is
            when rst_state =>
                SPTP_ENABLE<='0';
                SPTP_MSFLAG<='0';
                SPTP_START_SYNC<='0';
                SPTP_ADDR<=(others=>'0');
            when idle =>
                SPTP_ENABLE<='0';
                SPTP_MSFLAG<=ismaster;
                SPTP_START_SYNC<='0';
                SPTP_ADDR<=caddr;
            when send_sync =>
                SPTP_ENABLE<='1';
                SPTP_MSFLAG<=ismaster;
                SPTP_START_SYNC<='1';
                SPTP_ADDR<=caddr;
            when next_sync =>
                SPTP_ENABLE<='0';
                SPTP_MSFLAG<=ismaster;
                SPTP_START_SYNC<='0';
                SPTP_ADDR<=caddr;
            when others =>
                SPTP_ENABLE<='0';
                SPTP_MSFLAG<=ismaster;
                SPTP_START_SYNC<='0';
                SPTP_ADDR<=(others=>'0');
        end case;
    end process;

    --next state process
    process(cstate, SPTP_CTRDY)
    begin
        case cstate is 
            when rst_state=>
                nstate<=idle;
            when idle =>
                if ismaster='1' then
                    nstate<=send_sync;
                else
                    nstate<=idle;
                end if;
            when send_sync =>
                nstate<=next_sync;
            when next_sync =>
                if SPTP_CTRDY='1' then
                   nstate<=idle;
                else 
                   nstate<=next_sync;
                end if;
            when others =>
                   nstate<=rst_state;
        end case;
    end process



end Behavioral ;