----------------------------------------------------------------------------------
-- Company: MLAB-ICTP
-- Engineer: L. Garcia
-- 
-- Create Date: 22/06/2021 07:24:58 PM
-- Design Name: HVIBNI
-- Module Name: HVIBNI_driver - Behavioral
-- Project Name: HVIBNI
-- Target Devices: zynq 7030
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity HVIBNI_driver is
    Generic(
        tdelay : integer:= 500
    );
    Port(
        DIN     : in STD_LOGIC;
        DOUT    : out STD_LOGIC;
        TDM_DIR : in STD_LOGIC;
        DCITERMDISABLE : in STD_LOGIC;

        TDM_O_P : INOUT STD_LOGIC;
        TDM_O_N : INOUT STD_LOGIC
       );
end HVIBNI_driver;

architecture Behavioral of HVIBNI_driver is

begin

IOBUFDS_DCIEN_inst : IOBUFDS_DCIEN
    generic map (
        DIFF_TERM => "TRUE", -- Differential termination (TRUE/FALSE)
        IBUF_LOW_PWR => "FALSE", -- Low Power - TRUE, HIGH Performance = FALSE
        IOSTANDARD => "DEFAULT", -- Specify the I/O standard
        SLEW => "FAST", -- Specify the output slew rate
        USE_IBUFDISABLE => "TRUE") -- Use IBUFDISABLE function "TRUE" or "FALSE"
    port map (
        O => DOUT, -- Buffer output
        IO => TDM_O_P, -- Diff_p inout (connect directly to top-level port)
        IOB => TDM_O_N, -- Diff_n inout (connect directly to top-level port)
        DCITERMDISABLE => DCITERMDISABLE, -- DCI Termination enable input
        I => DIN, -- Buffer inputkcica
        IBUFDISABLE => TDM_DIR, -- Input disable input, low=disable
        T => TDM_DIR 
    );

end Behavioral;