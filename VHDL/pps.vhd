library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

entity pps_component is
  generic(CLK_FREQ : INTEGER :=250000000;
  CBITS : INTEGER :=28;
  CLK_RES : INTEGER :=28;
  TRES : INTEGER := 32); --Freq in Mhz
  port (
    CLK  : IN STD_LOGIC;
    RSTN : IN STD_LOGIC;

    LOCAL_TIME : IN STD_LOGIC_VECTOR(TRES -1 DOWNTO 0);

    pps : OUT STD_LOGIC
  ) ;
end pps_component ; 

architecture arch of pps_component is

    signal pps_signal : std_logic;
begin

-- process(CLK, RSTN)
-- begin
--     if RSTN = '0' THEN
--         pps_signal<='0';
--     elsif rising_edge(CLK) then
--         if to_integer(unsigned(LOCAL_TIME(CBITS-1 DOWNTO 0))) = 0 then
--             pps_signal<=not pps_signal;
--         end if;
--     end if;
-- END PROCESS;

-- pps<=pps_signal;
pps<=LOCAL_TIME(CBITS);

end architecture ;