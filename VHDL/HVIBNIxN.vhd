----------------------------------------------------------------------------------
-- Company: MLAB-ICTP
-- Engineer: L. Garcia
-- 
-- Create Date: 22/06/2021 07:24:58 PM
-- Design Name: HVIBNI
-- Module Name: HVIBNIxN_driver - Behavioral
-- Project Name: HVIBNI
-- Target Devices: zynq 7030
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity HVIBNIxN_driver is
    Generic(
        N : integer :=4;
        tdelay : integer:= 500
    );
    Port(
        DIN     : in STD_LOGIC_VECTOR(N-1 downto 0); --! Data Input
        DOUT    : out STD_LOGIC_VECTOR(N-1 downto 0); --! Data Output
        TDM_DIR : in STD_LOGIC_VECTOR(N-1 downto 0); --! Transmission Direction
        DCITERMDISABLE : in STD_LOGIC_VECTOR(N-1 downto 0); --! Controlled Impedance Terminal enable

        TDM_O_P : INOUT STD_LOGIC_VECTOR(N-1 downto 0);
        TDM_O_N : INOUT STD_LOGIC_VECTOR(N-1 downto 0);

        TDM_O_EN : OUT STD_LOGIC_VECTOR(N-1 downto 0)
       );
end HVIBNIxN_driver;

architecture Behavioral of HVIBNIxN_driver is

signal dout_sig, in_disable : STD_LOGIC_VECTOR(N-1 DOWNTO 0);
begin

TDM_O_EN<=TDM_DIR;
DOUT<=dout_sig and TDM_DIR;
in_disable<=(not TDM_DIR);

    HVIBNI_REGS: FOR i in 0 to N-1 generate
    begin
        IOBUFDS_DCIEN_inst_X : IOBUFDS_DCIEN
            generic map (
                DIFF_TERM => "TRUE", -- Differential termination (TRUE/FALSE)
                IBUF_LOW_PWR => "FALSE", -- Low Power - TRUE, HIGH Performance = FALSE
                IOSTANDARD => "DEFAULT", -- Specify the I/O standard
                SLEW => "FAST", -- Specify the output slew rate
                USE_IBUFDISABLE => "TRUE") -- Use IBUFDISABLE function "TRUE" or "FALSE"
            port map (
                O => dout_sig(i), -- Buffer output
                IO => TDM_O_P(i), -- Diff_p inout (connect directly to top-level port)
                IOB => TDM_O_N(i), -- Diff_n inout (connect directly to top-level port)
                DCITERMDISABLE => DCITERMDISABLE(i), -- DCI Termination enable input
                I => DIN(i), -- Buffer inputkcica
                IBUFDISABLE => in_disable(i), -- Input disable input, low=disable
                T => TDM_DIR(i) 
            );
    END generate HVIBNI_REGS;

end Behavioral;