library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

entity timeout is
  port (
    CLK : IN STD_LOGIC;
    RSTN : IN STD_LOGIC;
    START : IN STD_LOGIC;
    CLEAR : IN STD_LOGIC;

    COUNT : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    DONE  : OUT STD_LOGIC
  ) ;
end timeout ; 

architecture arch of timeout is

signal count_sig : integer;
type state  is (idle, counting, cend);
signal cstate, nstate : state;

begin

    process(CLK, RSTN, START, CLEAR)
    begin
        if RSTN = '0' then
            cstate<=idle;
        elsif rising_edge(clk) then 
            if CLEAR = '1' then 
                cstate<=idle;
            else
                cstate<=nstate;
            end if;
        end if;
    end process;


    process(CLK, RSTN)
    begin
        if RSTN='0' then
            count_sig<=0;
        elsif rising_edge(CLK) then
            if cstate = counting then
                count_sig<=count_sig+1;
            else
                count_sig<=0;
            end if;
        end if;
    end process;



    process(clk, cstate)
    begin
        case cstate is 
            when idle =>
                DONE<='0';
            when counting =>
                DONE<='0';
            when cend =>
                DONE<='1';
        end case;            
    end process;

    process(cstate, START, COUNT)
    begin
        case cstate is
            when idle =>
                if START = '1' then
                    nstate<=counting;
                else 
                    nstate<=idle;
                end if;
            when counting =>
                if START = '0' then
                    nstate<=idle;
                elsif count_sig <= to_integer(unsigned(COUNT)) - 1 then
                    nstate<=counting;
                else
                    nstate <=cend;
                end if;
            when cend =>
                if START = '0' then
                    nstate<=idle;
                else 
                    nstate<=cend;
                end if;
        end case;
    end process;

end architecture ;