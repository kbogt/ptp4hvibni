connect -url tcp:LN-ASSOC-6:3121
source /opt/my_gits/ptp4hvibni/SDK/Top_wrapper_hw_platform_0/ps7_init.tcl
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent JTAG-HS2 210249AE8C3E"} -index 0
rst -system
after 3000
targets -set -filter {jtag_cable_name =~ "Digilent JTAG-HS2 210249AE8C3E" && level==0} -index 1
fpga -file /opt/my_gits/ptp4hvibni/SDK/Top_wrapper_hw_platform_0/Top_wrapper.bit
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent JTAG-HS2 210249AE8C3E"} -index 0
loadhw -hw /opt/my_gits/ptp4hvibni/SDK/Top_wrapper_hw_platform_0/system.hdf -mem-ranges [list {0x40000000 0xbfffffff}]
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent JTAG-HS2 210249AE8C3E"} -index 0
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Digilent JTAG-HS2 210249AE8C3E"} -index 0
dow /opt/my_gits/ptp4hvibni/SDK/slave_test/Debug/slave_test.elf
configparams force-mem-access 0
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Digilent JTAG-HS2 210249AE8C3E"} -index 0
con
